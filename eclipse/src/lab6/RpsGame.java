package lab6;
import java.util.Random;
/**
 * 
 * @author David
 *
 */
public class RpsGame {
	private Random rng;
	private int wins;
	private int ties;
	private int losses;
	
	public RpsGame() {
		this.rng = new Random();
		this.wins = 0;
		this.ties = 0;
		this.losses = 0;
	}
	
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		return this.losses;
	}
	public String playRound(String playerChoice) {
		int computerChoiceInt = rng.nextInt(3);
		String computerChoice = "";
		String result = "";
		if(computerChoiceInt == 0) {
			computerChoice = "Rock";
		}else if(computerChoiceInt == 1) {
			computerChoice = "Scissors";
		}else if(computerChoiceInt == 2) {
			computerChoice = "Paper";
		}
		
		if(computerChoice.equals(playerChoice)) {
			result = "Computer chose " + computerChoice + " and it is a tie!";
			ties += 1;
		} else if(computerChoice.equals("Rock") && playerChoice.equals("Scissors")||
				  computerChoice.equals("Paper") && playerChoice.equals("Rock")||
				  computerChoice.equals("Scissors") && playerChoice.equals("Paper")) {
			result = "Computer chose " + computerChoice + " and the computer won!";
			losses += 1;
		} else if(computerChoice.equals("Scissors") && playerChoice.equals("Rock")||
				  computerChoice.equals("Paper") && playerChoice.equals("Scissors")||
				  computerChoice.equals("Rock") && playerChoice.equals("Paper")) {
			result = "Computer chose " + computerChoice + " and the user won!";
			wins += 1;
		}
		
		return result;
	}
}
