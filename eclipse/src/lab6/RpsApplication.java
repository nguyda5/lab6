/**
 * @author David Nguyen
 */

package lab6;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	private RpsGame rps = new RpsGame();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		
		
		
		TextField message = new TextField("Welcome!");
		//200 width is still too short
		message.setPrefWidth(300);
		TextField winsText = new TextField("Wins: 0");
		TextField lossesText = new TextField("Losses: 0");
		TextField tiesText = new TextField("Ties: 0");

		HBox textFields = new HBox();
		textFields.getChildren().addAll(message,winsText,lossesText,tiesText);
		
		Button rockButton = new Button("Rock");
		RpsChoice rockChoice = new RpsChoice(rps, message, winsText, lossesText, tiesText, "Rock");
		rockButton.setOnAction(rockChoice);
		
		Button scissorsButton = new Button("Scissors");
		RpsChoice scissorChoice = new RpsChoice(rps, message, winsText, lossesText, tiesText, "Scissors");
		scissorsButton.setOnAction(scissorChoice);
		
		Button paperButton = new Button("Paper");
		RpsChoice paperChoice = new RpsChoice(rps, message, winsText, lossesText, tiesText, "Paper");
		paperButton.setOnAction(paperChoice);
		
		HBox buttons = new HBox();
		buttons.getChildren().addAll(rockButton,scissorsButton,paperButton);
		
		VBox overall = new VBox();
		overall.getChildren().addAll(buttons,textFields);
		
		root.getChildren().add(overall);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    


