
/**
 * @author David Nguyen
 */
package lab6;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
	private RpsGame rps;
	private TextField message;
	private TextField winsText;
	private TextField lossesText;
	private TextField tiesText;
	private String playerChoice;
	public RpsChoice(RpsGame rps, TextField message, TextField winsText, TextField lossesText, TextField tiesText, String playerChoice) {
		this.rps = rps;
		this.message = message;
		this.winsText = winsText;
		this.lossesText = lossesText;
		this.tiesText = tiesText;
		this.playerChoice = playerChoice;
	}
	@Override
	public void handle(ActionEvent e) {
		String result = rps.playRound(playerChoice);
		message.setText(result);
		winsText.setText("Wins: " + rps.getWins());
		lossesText.setText("Losses: " + rps.getLosses());
		tiesText.setText("Ties: " + rps.getTies());
	}
}
